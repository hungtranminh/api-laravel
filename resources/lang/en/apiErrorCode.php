<?php
$APICode = array(

    //create user begin
    'no_user' => 'Cant find user',
    'no_post' => 'No post',
    'token_error' => 'Unauthorized',
    'token_expired' => 'Expired Token',

    'name_required' => 'The your name field is required.',
    'name_string' => 'The your name must be a string',
    'name_min' => 'The your name must be at least 2 characters',
    'email_required' => 'The your email field is required.',
    'email_regex' => 'The your email must be at least 2 characters before and after @',
    'email_email' => 'The your email must be a valid email address',
    'email_unique' => 'The your email has already been taken',
    'phone_number_required' => 'The your phone number field is required.',
    'phone_number_min' => 'The your phone number must be at least 9 characters',
    'phone_number_unique' => 'The your phone number has already been taken',
    'password_required' => 'The your password field is required.',
    'password_string' => 'The your password must be a string',
    'password_min' => 'The your password must be at least 6 characters',
    'password_confirmed' => 'The password confirmation does not match',
    'dob_required' => 'The your dob field is required.',
    'dob_date' => 'The your dob does not match the format YYYY-mm-dd.',
    'zip_code_required' => 'The your zip code field is required.',
    'lat_required' => 'The your latitude field is required.',
    'lat_numeric' => 'The latitude must be a number.',
    'long_required' => 'The your longitude field is required.',
    'long_numeric' => 'The longitude must be a number.',
    'gender_required' => 'The your gender field is required.',
//    'account_required' => 'The account field is required',
//    'account_min' => 'The account must be at least 10 characters',

    'already_friends' => 'You can not add friend yourself',
    'be_friends' => 'This user is friended by you',
    'min_age_required' => 'The min age field is required.',
    'min_age_integer' => 'The min age must be an integer.',
    'min_age_min' => 'The min age must be at least 0.',
    'max_age_required' => 'The max age field is required.',
    'max_age_integer' => 'The min age must be an integer.',
    'max_age_min' => 'The  max age must be at least 1.',
);
$apiErrorCodes = array(
    //create user begin
    'no_user' => 1000,
    'no_post' => 1001,
    'token_error' => 2000,
    'token_expired' => 2001,

    'name_required' => '1510',
    'name_string' => '1511',
    'name_min' => '1512',
    'email_required' => '1520',
    'email_regex' => '1522',
    'email_email' => '1523',
    'email_unique' => '1524',
    'phone_number_required' => '1530',
    'phone_number_min' => '1532',
    'phone_number_unique' => '1533',
    'password_required' => '1540',
    'password_string' => '1541',
    'password_min' => '1542',
    'password_confirmed' => '1543',
    'dob_required' => '1550',
    'dob_date' => '1551',
    'zip_code_required' => '1560',
    'lat_required' => '1570',
    'lat_numeric' => '1571',
    'long_required' => '1580',
    'long_numeric' => '1581',
    'gender_required' => '1590',

//    'account_required' => '1610',
//    'account_min' => '1611',

    'already_friends' => '2510',
    'really_friends' => '2511',
    'min_age_required' => '2520',
    'min_age_integer' => '2521',
    'min_age_min' => '2522',
    'max_age_required' => '2530',
    'max_age_integer' => '2531',
    'max_age_min' => '2532',
);
$APICode['ApiErrorCodes'] = $apiErrorCodes;
$apiErrorCodesFlip = array_flip($apiErrorCodes);
$APICode['ApiErrorCodesFlip'] = $apiErrorCodesFlip;

return $APICode;