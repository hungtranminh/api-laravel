<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone_number', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function posts()
    {
        return $this->hasMany('App\Post','id_user','id');
    }

    public function tokens()
    {
        return $this->hasMany('App\Token','id_user','id');
    }

    public function password_resets()
    {
        return $this->hasMany('App\ResetPassword','id_user','id');
    }

    public function relationshipUser()
    {
        return $this->hasMany('App\Relationship','id_user','id');
    }

    public function relationshipFriend()
    {
        return $this->hasMany('App\Relationship','id_friend','id');
    }
}
