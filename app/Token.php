<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    protected $table = "tokens";

    public function users()
    {
        return $this->belongsTo('App\User','id_user','id');
    }

    public function generateToken()
    {
        $this->token = str_random(128);
        $this->expired_at = Carbon::now()->addHours('2');
        $this->save();

        return $this->token;
    }
}
