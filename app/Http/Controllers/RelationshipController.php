<?php

namespace App\Http\Controllers;

use App\Relationship;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class RelationshipController extends Controller
{
    /**
     * @SWG\Post(
     *   path="/add_friend",
     *   summary="Add Friend",
     *   @SWG\Parameter(
     *     name="email",
     *     in="query",
     *     description="Your Email",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="A list with products"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     *     security={
     *       {"api_key": {}}
     *     }
     * )
     */

    public function addFriend(Request $request)
    {
        try {
            $errorCode = $this->apiErrorCodes;

            $user = $request->attributes->get('user_auth');

            $user_friend = User::where('email', $request->email)->first();


            if (!isset($user_friend)) {
                return $this->respondWithErrorMessage($errorCode['no_user'], $errorCode['ApiErrorCodes']['no_user'], 401);
            } elseif ($user->id == $user_friend->id) {
                return $this->respondWithErrorMessage($errorCode['already_friends'], $errorCode['ApiErrorCodes']['already_friends'], 401);
            }

            $list_friends = Relationship::where('id_user', $user->id)->get();

            foreach ($list_friends as $item) {
                if ($item->id_friend == $user_friend->id) {
                    return $this->respondWithErrorMessage($errorCode['be_friends'], $errorCode['ApiErrorCodes']['really_friends'], 401);
                }
            }

            $friend = new Relationship();
            $friend->id_user = $user->id;
            $friend->id_friend = $user_friend->id;
            $friend->save();

            $auto_add = new Relationship();
            $auto_add->id_user = $user_friend->id;
            $auto_add->id_friend = $user->id;
            $auto_add->save();

            return $this->respondWithSuccess('Added successfully', 200);

        } catch (\Exception $exception) {
            return $this->errorInternalError();
        }
    }

    /**
     * @SWG\Post(
     *   path="/find_friend",
     *   summary="Find Friend",
     *   @SWG\Parameter(
     *     name="gender",
     *     in="query",
     *     description="Gender  (m - male, f - female, u - unknown)",
     *     required=true,
     *     type="string",
     *     enum={"m","f","u"},
     *   ),
     *   @SWG\Parameter(
     *     name="min_age",
     *     in="query",
     *     description="Min Age",
     *     type="integer",
     *   ),
     *   @SWG\Parameter(
     *     name="max_age",
     *     in="query",
     *     description="Max Age",
     *     type="integer",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="A list with products"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     *     security={
     *       {"api_key": {}}
     *     }
     * )
     */
    public function findFriend(Request $request)
    {
        try {
        $validator = Validator::make(
            $request->all(),
            [
                'gender' => 'required',
                'min_age' => 'required|integer|min:0',
                'max_age' => 'required|integer|min:1'
            ],
            [
                'gender.required' => '1590',
                'min_age.required' => '2520',
                'min_age.integer' => '2521',
                'min_age.min' => '2522',
                'max_age.required' => '2530',
                'max_age.integer' => '2531',
                'max_age.min' => '2532',
            ]);

        $errors = $validator->errors();

        $arr = array();
        foreach ($errors->all() as $message) {
            array_push($arr,
                ["messaage" => $this->apiErrorCodes[$this->apiErrorCodes['ApiErrorCodesFlip'][$message]],
                    "code" => $message]);
        }

        $response = array(
            'error' => true,
            'data' => null,
            'errors' => $arr
        );

        if ($errors->any()) {
            return response()->json($response, 401);
        }
        $user = $request->attributes->get('user_auth');

        $result = User::selectRaw(
            "id, name, gender, email, zip_code, phone_number, lat, lng,
            (((acos(sin((?*pi()/180))
            *sin((`lat`*pi()/180))+cos((?*pi()/180))
            *cos((`lat`*pi()/180))
            *cos(((?- `lng`)*pi()/180))))*180/pi())*60*1.1515*1.609344) as distance,(YEAR(CURDATE())- YEAR(dob)) AS age")
            ->setBindings([$user->lat, $user->lat, $user->lng])
            ->where('id', '<>', $user->id)
            ->where('gender', $request->gender)
            ->whereRaw('(YEAR(CURRENT_DATE())-YEAR(dob)) >= ?', $request->min_age)
            ->whereRaw('(YEAR(CURRENT_DATE())-YEAR(dob)) <= ?', $request->max_age)
            ->orderBy("distance")
            ->get();
        //->paginate(5); bi loi

//        cach khac sort theo khoang cach
//        $result =
//                User::select(
//                    DB::raw("id, name, gender, email, zip_code, phone_number, lat, lng, (
//                    3959 * acos(
//                        cos( radians(  ?  ) ) *
//                        cos( radians( lat ) ) *
//                        cos( radians( lng ) - radians(?) ) +
//                        sin( radians(  ?  ) ) *
//                        sin( radians( lat ) )
//                    )
//                )AS distance"))
//                    ->setBindings([$user->lat, $user->lng, $user->lat])
//                    ->whereNotIn('id',[$user->id])
//                    ->orderBy("distance")
//                    ->paginate(5);

        if (empty($result))
            return response()->json(array("error" => false, "data" => null, "errors" => null));

        return $this->respondWithSuccess($result);
        } catch (\Exception $exception) {
            return $this->errorInternalError();
        }
    }
}
