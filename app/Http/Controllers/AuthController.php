<?php

namespace App\Http\Controllers;

use App\ResetPassword;
use App\Token;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Mockery\Exception;

class AuthController extends Controller
{
    /**
     * @SWG\Post(
     *   path="/login",
     *   summary="Login",
     *   @SWG\Parameter(
     *     name="type",
     *     in="query",
     *     description="Select the login method",
     *     required=true,
     *     type="string",
     *     enum={"email","phone_number"},
     *   ),
     *   @SWG\Parameter(
     *     name="email",
     *     in="query",
     *     description="Your Email",
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="phone_number",
     *     in="query",
     *     description="Your Phone Number",
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="password",
     *     in="query",
     *     description="Your Password",
     *     required=true,
     *     type="string",
     *     format="password",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="A list with products"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   )
     * )
     */
    public function login(Request $request)
    {
        try {
            if ($request->get('type') == "email") {

                $validator = Validator::make(
                    $request->all(),
                    [
                        'email' => 'required|email|regex:/^[a-zA-Z0-9\.]{2,}@[a-z0-9]{2,}\.[a-z\.]{2,}$/',
                        'password' => 'required|string|min:6',
                    ],
                    [
                        'email.required' => '1520',
                        'email.regex' => '1522',
                        'email.email' => '1523',
                        'password.required' => '1540',
                        'password.string' => '1541',
                        'password.min' => '1542',
                    ]);

                $errors = $validator->errors();

                $arr = array();
                foreach ($errors->all() as $message) {
                    array_push($arr,
                        ["messaage" => $this->apiErrorCodes[$this->apiErrorCodes['ApiErrorCodesFlip'][$message]],
                            "code" => $message]);
                }

                $response = array(
                    'error' => true,
                    'data' => null,
                    'errors' => $arr
                );

                if ($errors->any()) {
                    return response()->json($response, 401);
                }

                $email = $request->get('email');
                $password = $request->get('password');

                $user = User::where('email', $email)->first();

                $errorCode = $this->apiErrorCodes;

                if (isset($user) && (password_verify($password, $user->password))) {

                    $token = new Token();
                    $token->id_user = $user->id;
                    $token->generateToken();

                    $data = Token::where('tokens.token', $token->token)
                        ->join('users', 'tokens.id_user', '=', 'users.id')
                        ->select('users.*', 'tokens.token')
                        ->get();
                    return $this->respondWithSuccess($data);
                } else
                    return $this->respondWithErrorMessage($errorCode['no_user'], $errorCode['ApiErrorCodes']['no_user'], 401);

            } elseif ($request->get('type') == "phone_number") {

                $validator = Validator::make(
                    $request->all(),
                    [
                        'phone_number' => 'required|min:9',
                        'password' => 'required|string|min:6',
                    ],
                    [
                        'phone_number.required' => '1530',
                        'phone_number.min' => '1532',
                        'password.required' => '1540',
                        'password.string' => '1541',
                        'password.min' => '1542',
                    ]);

                $errors = $validator->errors();

                $arr = array();
                foreach ($errors->all() as $message) {
                    array_push($arr,
                        ["messaage" => $this->apiErrorCodes[$this->apiErrorCodes['ApiErrorCodesFlip'][$message]],
                            "code" => $message]);
                }

                $response = array(
                    'error' => true,
                    'data' => null,
                    'errors' => $arr
                );

                if ($errors->any()) {
                    return response()->json($response, 401);
                }

                $phone_number = $request->get('phone_number');
                $password = $request->get('password');

                $user = User::where('phone_number', $phone_number)->first();

                $errorCode = $this->apiErrorCodes;

                if (isset($user) && (password_verify($password, $user->password))) {

                    $token = new Token();
                    $token->id_user = $user->id;
                    $token->generateToken();

                    $data = Token::where('tokens.token', $token->token)
                        ->join('users', 'tokens.id_user', '=', 'users.id')
                        ->select('users.*', 'tokens.token')
                        ->get();
                    return $this->respondWithSuccess($data);
                } else
                    return $this->respondWithErrorMessage($errorCode['no_user'], $errorCode['ApiErrorCodes']['no_user'], 401);
            }
        } catch (\Exception $exception) {
            return $this->errorInternalError();
        }
    }


    /**
     * @SWG\Post(
     *   path="/password/sendMail",
     *   summary="Send Mail Reset Password",
     *   @SWG\Parameter(
     *     name="email",
     *     in="query",
     *     description="Your Email",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="A list with products"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   )
     * )
     */
    public function sendMail(Request $request)
    {
        try {
            $errorCode = $this->apiErrorCodes;

            $validator = Validator::make(
                $request->all(),
                [
                    'email' => 'required|email|regex:/^[a-zA-Z0-9\.]{2,}@[a-z0-9]{2,}\.[a-z\.]{2,}$/',
                ],
                [
                    'email.required' => '1520',
                    'email.regex' => '1522',
                    'email.email' => '1523',
                ]);

            $errors = $validator->errors();

            $arr = array();
            foreach ($errors->all() as $message) {
                array_push($arr,
                    ["messaage" => $this->apiErrorCodes[$this->apiErrorCodes['ApiErrorCodesFlip'][$message]],
                        "code" => $message]);
            }

            $response = array(
                'error' => true,
                'data' => null,
                'errors' => $arr
            );
            if ($errors->any()) {
                return response()->json($response, 401);
            }

            $user = User::where('email', $request->email)->first();

            if (isset($user)) {
                $code_reset = new ResetPassword();
                $code_reset->id_user = $user->id;
                $code_reset->generateCode();

                $data = array('url' => 'http://localhost/api-laravel/public/api/password/reset/' . $code_reset->code, 'mail' => $user->email);

                Mail::send('emails.resetpassword', $data, function ($message) use ($data) {
                    $message->to($data['mail'])->subject('Reset Password');
                    $message->from('ad.resetpass@gmail.com', 'Admin');
                });

                $response = array(
                    'error' => false,
                    'data' => "Submitted successfully",
                    'errors' => null);
                return response()->json($response, 200);
            } else {
                return $this->respondWithErrorMessage($errorCode['no_user'], $errorCode['ApiErrorCodes']['no_user'], 401);
            }
        } catch (Exception $exception) {
            return $this->errorInternalError('Internal Server Error');
        }
    }

    public function getNewPassword($code)
    {
        try {
            $code_reset = ResetPassword::where('code', $code)->first();

            return view('resetpassword', ['code_reset' => $code_reset]);
        } catch (\Exception $exception) {
            return $this->errorInternalError();
        }
    }

    public function postNewPassword(Request $request, $code)
    {
        try {
            $code_reset = ResetPassword::where('code', $code)->first();

            $errorCode = $this->apiErrorCodes;

            $validator = Validator::make(
                $request->all(),
                [
                    'password' => 'required|string|min:6|confirmed',
                ],
                [
                    'password.required' => '1540',
                    'password.string' => '1541',
                    'password.min' => '1542',
                    'password.confirmed' => '1543',
                ]);

            $errors = $validator->errors();

            $arr = array();
            foreach ($errors->all() as $message) {
                array_push($arr,
                    ["messaage" => $this->apiErrorCodes[$this->apiErrorCodes['ApiErrorCodesFlip'][$message]],
                        "code" => $message]);
            }

            $response = array(
                'error' => true,
                'data' => null,
                'errors' => $arr
            );
            if ($errors->any()) {
                return response()->json($response, 401);
            }

            if (!isset($code_reset)) {
                return $this->respondWithErrorMessage($errorCode['token_error'], $errorCode['ApiErrorCodes']['token_error'], 401);
            } elseif (Carbon::now() > $code_reset->expired_at) {
                return $this->respondWithErrorMessage($errorCode['token_expired'], $errorCode['ApiErrorCodes']['token_expired'], 401);
            }

            $user = User::where('id', $code_reset->id_user)->first();

            $user->password = bcrypt($request->password);
            $user->save();

            return $this->respondWithSuccess('Updated successfully', 200);
        } catch (\Exception $exception) {
            return $this->errorInternalError();
        }
    }

    /**
     * @SWG\Post(
     *   path="/logout",
     *   summary="Logout",
     *     @SWG\Response(
     *     response=200,
     *     description="A list with products"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     *     security={
     *       {"api_key": {}}
     *     }
     * )
     */
    public function logout(Request $request)
    {
        $auth = $request->headers->get('Authorziation');

        $token = Token::where('token', $auth)->first();
        $token->expired_at = Carbon::now();
        $token->save();

        return response()->json(array("error" => false, "data" => null, "errors" => null));
    }
}
