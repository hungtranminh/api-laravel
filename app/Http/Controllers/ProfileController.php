<?php
/**
 * @SWG\Swagger(
 *   basePath="/api-laravel/public/api",
 *   @SWG\Info(
 *     title="MyApp API",
 *     version="0.2"
 *   )
 * )
 * @SWG\SecurityScheme(
 *   securityDefinition="api_key",
 *   type="apiKey",
 *   in="header",
 *   name="Authorziation"
 * )
 */


namespace App\Http\Controllers;

use App\Post;
use App\Token;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    /**
     * @SWG\Get(
     *   path="/user",
     *   summary="get profile",
     *   @SWG\Response(
     *     response=200,
     *     description="A list with products"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     *     security={
     *       {"api_key": {}}
     *     }
     * )
     */
    public function getProfile(Request $request)
    {
        $errorCode = $this->apiErrorCodes;

        if (empty($request->attributes->get('user_auth'))) {
            return $this->respondWithErrorMessage($errorCode['no_user'], $errorCode['ApiErrorCodes']['no_user'], 401);
        } else {
            return $this->respondWithSuccess($request->attributes->get('user_auth'));
        }
    }

    /**
     * @SWG\Post(
     *   path="/register",
     *   summary="Create User",
     *   @SWG\Parameter(
     *     name="name",
     *     in="query",
     *     description="User Name",
     *     required=true,
     *     type="string",
     *   ),
     *     @SWG\Parameter(
     *     name="dob",
     *     in="query",
     *     description="User Day of birth (yyyy-MM-dd)",
     *     required=true,
     *     type="string",
     *     format="date",
     *   ),
     *     @SWG\Parameter(
     *     name="gender",
     *     in="query",
     *     description="User Gender (m - male, f - female, u - unknown)",
     *     required=true,
     *     type="string",
     *     enum={"m","f","u"},
     *   ),
     *   @SWG\Parameter(
     *     name="email",
     *     in="query",
     *     description="Your Email",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="zip_code",
     *     in="query",
     *     description="Your Zip Code",
     *     required=true,
     *     type="integer",
     *   ),
     *   @SWG\Parameter(
     *     name="phone_number",
     *     in="query",
     *     description="Your Phone Number",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="lat",
     *     in="query",
     *     description="Your Latitude",
     *     required=true,
     *     type="number",
     *     format="float",
     *   ),
     *   @SWG\Parameter(
     *     name="long",
     *     in="query",
     *     description="Your Longitude",
     *     required=true,
     *     type="number",
     *     format="float",
     *   ),
     *   @SWG\Parameter(
     *     name="password",
     *     in="query",
     *     description="Your Password",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="password_confirmation",
     *     in="query",
     *     description="Your Password Again",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="A list with products"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   )
     * )
     */
    public function register(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|string|min:2',
                'email' => 'required|email|regex:/^[a-zA-Z0-9\.]{2,}@[a-z0-9]{2,}\.[a-z\.]{2,}$/|unique:users',
                'phone_number' => 'required|min:9|unique:users',
                'password' => 'required|string|min:6|confirmed',
                'dob' => 'required|date',
                'zip_code' => 'required',
                'lat' => 'required|numeric',
                'long' => 'required|numeric',
                'gender' => 'required',
            ],
            [
                'name.required' => '1510',
                'name.string' => '1511',
                'name.min' => '1512',
                'email.required' => '1520',
                'email.regex' => '1522',
                'email.email' => '1523',
                'email.unique' => '1524',
                'phone_number.required' => '1530',
                'phone_number.min' => '1532',
                'phone_number.unique' => '1533',
                'password.required' => '1540',
                'password.string' => '1541',
                'password.min' => '1542',
                'password.confirmed' => '1543',
                'dob.required' => '1550',
                'dob.date' => '1551',
                'zip_code.required' => '1560',
                'lat.required' => '1570',
                'lat.numeric' => '1571',
                'long.required' => '1580',
                'long.numeric' => '1581',
                'gender.required' => '1590',
            ]);

        $errors = $validator->errors();

        $arr = array();
        foreach ($errors->all() as $message) {
            array_push($arr,
                ["messaage" => $this->apiErrorCodes[$this->apiErrorCodes['ApiErrorCodesFlip'][$message]],
                    "code" => $message]);
        }

        $response = array(
            'error' => true,
            'data' => null,
            'errors' => $arr
        );
        if ($errors->any()) {
            return response()->json($response, 401);
        }

        $user = new User();

        $user->name = $request->name;
        $user->dob = $request->dob;
        $user->gender = $request->gender;
        $user->email = $request->email;
        $user->zip_code = $request->zip_code;
        $user->phone_number = $request->phone_number;
        $user->lat = $request->lat;
        $user->long = $request->long;
        $user->password = bcrypt($request->password);

        $user->save();

        return $this->respondWithSuccess($user);
    }
}
