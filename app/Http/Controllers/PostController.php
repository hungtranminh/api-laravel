<?php

namespace App\Http\Controllers;

use App\Post;
use App\Relationship;
use App\Token;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use Psy\Output\ProcOutputPager;

class PostController extends Controller
{
    /**
     * @SWG\Get(
     *   path="/post",
     *   summary="list post",
     *   @SWG\Response(
     *     response=200,
     *     description="A list with products"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     *     security={
     *       {"api_key": {}}
     *     }
     * )
     */
    public function getListPost(Request $request)
    {
        try {
            $user = $request->attributes->get('user_auth');

            $posts = DB::table('posts')
                ->join('relationship', 'posts.id_user', '=', 'relationship.id_friend')
                ->join('users', 'posts.id_user', '=', 'users.id')
                ->where('relationship.id_user', $user->id)
//                ->orWhere('posts.id_user', $user->id)
                ->select('posts.*', 'users.name','users.gender','users.dob','users.zip_code','users.phone_number','users.email','users.lat','users.lng')
                ->get();

            if ($posts->isEmpty()) {
                return response()->json(array("error" => false, "data" => null, "errors" => null));
            } else {
                return $this->respondWithSuccess($posts);
            }
        } catch (Exception $exception) {
            return $this->errorInternalError();
        }
    }


    /**
     * @SWG\Get(
     *   path="/post/{post}",
     *   summary="get post",
     *   @SWG\Parameter(
     *     name="post",
     *     in="path",
     *     description="ID Post",
     *     required=true,
     *     type="integer",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="A list with products"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     *     security={
     *       {"api_key": {}}
     *     }
     * )
     */

    public function getPost(Request $request)
    {
        try {

            $user = $request->attributes->get('user_auth');

            $post = Post::where('id', $request->post)->first();


            if (empty($post)) {
                return response()->json(array("error" => false, "data" => null, "errors" => null));
            } else {

                if ($post->id_user == $user->id) {
                    return $this->respondWithSuccess($post);
                }

                $list_friends = Relationship::where('id_user', $user->id)->get();

                foreach ($list_friends as $item) {
                    if ($item->id_friend == $post->id_user) {
                        return $this->respondWithSuccess($post);
                    }
                }
            }
        } catch (\Exception $exception) {
            return $this->errorInternalError();
        }
    }

    /**
     * @SWG\Post(
     *   path="/post",
     *   summary="add post",
     *   @SWG\Parameter(
     *     name="post_content",
     *     in="query",
     *     description="Content",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="A list with products"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     *     security={
     *       {"api_key": {}}
     *     }
     * )
     */
    public function addPost(Request $request)
    {

        $post = new Post();
        $post->id_user = $request->attributes->get('user_auth')->id;
        $post->post_content = $request->post_content;
        $post->save();

        return $this->respondWithSuccess($post);

    }

    /**
     * @SWG\Put(
     *   path="/post/{post}",
     *   summary="update post",
     *   @SWG\Parameter(
     *     name="post",
     *     in="path",
     *     description="ID Post",
     *     required=true,
     *     type="integer",
     *   ),
     *   @SWG\Parameter(
     *     name="post_content",
     *     in="query",
     *     description="Content",
     *     required=true,
     *     type="string",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="A list with products"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     *     security={
     *       {"api_key": {}}
     *     }
     * )
     */

    public function updatePost(Request $request)
    {
        $errorCode = $this->apiErrorCodes;

        $post = Post::where('id', $request->post)->first();
        if ($post->id_user != $request->attributes->get('user_auth')->id) {
            return $this->respondWithErrorMessage($errorCode['token_error'], $errorCode['ApiErrorCodes']['token_error'], 401);
        }

        $post->id_user = $request->attributes->get('user_auth')->id;
        $post->post_content = $request->post_content;
        $post->save();

        return $this->respondWithSuccess($post);
    }


    /**
     * @SWG\Delete(
     *   path="/post/{post}",
     *   summary="delete post",
     *   @SWG\Parameter(
     *     name="post",
     *     in="path",
     *     description="ID Post",
     *     required=true,
     *     type="integer",
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="A list with products"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     *     security={
     *       {"api_key": {}}
     *     }
     * )
     */
    public function deletePost(Request $request)
    {
        $errorCode = $this->apiErrorCodes;

        $post = Post::where('id', $request->post)->first();
        if ($post->id_user != $request->attributes->get('user_auth')->id) {
            return $this->respondWithErrorMessage($errorCode['token_error'], $errorCode['ApiErrorCodes']['token_error'], 401);
        }

        $post->delete();

        return response()->json(array("error" => false, "data" => null, "errors" => null));
    }
}
