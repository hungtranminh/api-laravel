<?php

namespace App\Http\Middleware;

use App\Token;
use App\User;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class ApiAuthenticate
{
    public function __construct()
    {
        $this->apiErrorCode = Lang::get('apiErrorCode');
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response_token_error = array(
            "error" => true,
            "data" => null,
            "errors" => array([
                'errorCode' => $this->apiErrorCode['ApiErrorCodes']['token_error'],
                'errorMessage' => $this->apiErrorCode['token_error']
            ])
        );
        $response_token_expired = array(
            "error" => true,
            "data" => null,
            "errors" => array([
                'errorCode' => $this->apiErrorCode['ApiErrorCodes']['token_expired'],
                'errorMessage' => $this->apiErrorCode['token_expired']
            ])
        );

        $token = $request->headers->get('Authorziation');
        $auth = Token::where('token', $token)->first();
        if (!isset($auth)) {
            return response()->json($response_token_error, 401);
        } elseif (Carbon::now() > $auth->expired_at) {
            return response()->json($response_token_expired, 401);
        }
        $user = User::where('id',$auth->id_user)->first();
        $request->attributes->add(['user_auth'=>$user]);
        return $next($request);
    }
}
