<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ResetPassword extends Model
{
    protected $table = "password_resets";

    public function users()
    {
        return $this->belongsTo('App\User','id_user','id');
    }

    public function generateCode()
    {
        $this->code = str_random(128);
        $this->expired_at = Carbon::now()->addHours('2');
        $this->save();

        return $this->code;
    }
}
