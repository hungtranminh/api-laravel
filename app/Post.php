<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "posts";

    public function users()
    {
        return $this->belongsTo('App\User','id_user','id');
    }
}
