<?php

use Illuminate\Database\Seeder;

class RelationshipTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Relationship::truncate();

        $faker = Faker\Factory::create();

        for ($i=0; $i<10; $i++)
        {
            \App\Relationship::create([
                'id_user' => $faker->numberBetween(1,10),
                'id_friend' => $faker->numberBetween(1,10),
            ]);
        }
    }
}
