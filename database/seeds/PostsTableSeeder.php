<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Post::truncate();

        $faker = \Faker\Factory::create();

        for ($i=0; $i<20; $i++)
        {
            \App\Post::create([
                'id_user' => $faker->numberBetween(1,10),
                'post_content' => $faker->paragraph,
            ]);
        }
    }
}
