<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::truncate();

        $faker = \Faker\Factory::create();

        $password = bcrypt('111176');

        \App\User::create([
            'name' => 'Administrator',
            'dob'=> '1996-02-10',
            'gender'=>'m',
            'email' => 'admin@mail.abc',
            'zip_code'=>'84',
            'phone_number' => '01222222345',
            'lat'=>'10.797544',
            'long'=>'106.646869',
            'password' => $password,
        ]);

        for ($i = 0; $i < 10; $i++) {
            \App\User::create([
                'name' => $faker->name,
                'dob'=> $faker->dateTimeBetween('-30 years','now'),
                'gender'=>'m',
                'email' => $faker->email,
                'zip_code'=>$faker->randomNumber(3),
                'phone_number' => $faker->phoneNumber,
                'lat'=>$faker->randomNumber('2'),
                'long'=>$faker->randomNumber('3'),
                'password' => $password,
            ]);
        }
    }
}
