<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::post('register','ProfileController@register');

Route::post('login','AuthController@login');
Route::post('password/sendMail','AuthController@sendMail');
Route::get('password/reset/{code}','AuthController@getNewPassword');
Route::post('password/reset/{code}','AuthController@postNewPassword')->name('post.reset');

Route::middleware('api.auth')->group(function () {
    Route::post('logout','AuthController@logout');

    Route::get('user','ProfileController@getProfile');

    Route::post('add_friend','RelationshipController@addFriend');
    Route::post('find_friend','RelationshipController@findFriend');

    Route::get('post','PostController@getListPost');
    Route::post('post','PostController@addPost');
    Route::get('post/{post}','PostController@getPost');
    Route::put('post/{post}','PostController@updatePost');
    Route::delete('post/{post}','PostController@deletePost');
    });
